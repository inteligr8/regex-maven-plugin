package com.inteligr8.maven.regex;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.junit.Assert;
import org.junit.Test;

public class FileSetUnitTest {
	
	@Test
	public void srcMainJava() {
		FileSet fileset = new FileSet();
		fileset.setDirectory("src/main/java");
		fileset.setIncludes(Arrays.asList("**/*.java"));
		
		String fs = File.separator;
		FileSetManager fsman = new FileSetManager();
		Set<String> files = new HashSet<>(Arrays.asList(fsman.getIncludedFiles(fileset)));
		Assert.assertTrue(files.size() > 15);
		System.err.println(files);
		Assert.assertTrue(files.contains("com"+fs+"inteligr8"+fs+"maven"+fs+"regex"+fs+"AbstractFileMojo.java"));
	}

}
