package com.inteligr8.nio;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Assert;
import org.junit.Test;

public class DelimitedReadableByteChannelUnitTest {
	
	@Test
	public void empty() throws IOException, URISyntaxException {
		URL resource = this.getClass().getResource("/empty.file");
		Path path = Paths.get(resource.toURI());
		FileChannel fchannel = FileChannel.open(path);
		try {
			DelimitedReadableByteChannel drbchannel = new DelimitedReadableByteChannel(fchannel, Charset.defaultCharset(), "\n");
			try {
				StringBuilder strbuilder = new StringBuilder();
				Assert.assertEquals(-1L, drbchannel.read(strbuilder));
				Assert.assertEquals(0, strbuilder.length());
				Assert.assertNull(drbchannel.getLastDelimiterRead());
			} finally {
				drbchannel.close();
			}
		} finally {
			fchannel.close();
		}
	}
	
	@Test
	public void blankFirstLastLines() throws IOException, URISyntaxException {
		URL resource = this.getClass().getResource("/blank-first-last-lines.file");
		Path path = Paths.get(resource.toURI());
		FileChannel fchannel = FileChannel.open(path);
		try {
			DelimitedReadableByteChannel drbchannel = new DelimitedReadableByteChannel(fchannel, Charset.defaultCharset(), "\r\n", "\n");
			try {
				StringWriter writer = new StringWriter(1024);
				Assert.assertNotEquals(-1L, drbchannel.read(writer));
				Assert.assertEquals("", writer.toString());
				Assert.assertNotNull(drbchannel.getLastDelimiterRead());

				writer = new StringWriter(1024);
				Assert.assertNotEquals(-1L, drbchannel.read(writer));
				Assert.assertEquals("here is a line", writer.toString());
				Assert.assertNotNull(drbchannel.getLastDelimiterRead());

				writer = new StringWriter(1024);
				Assert.assertEquals(-1L, drbchannel.read(writer));
				Assert.assertNull(drbchannel.getLastDelimiterRead());
			} finally {
				drbchannel.close();
			}
		} finally {
			fchannel.close();
		}
	}
	
	@Test
	public void usedFirstLastLines() throws IOException, URISyntaxException {
		URL resource = this.getClass().getResource("/used-first-last-lines.file");
		Path path = Paths.get(resource.toURI());
		FileChannel fchannel = FileChannel.open(path);
		try {
			DelimitedReadableByteChannel drbchannel = new DelimitedReadableByteChannel(fchannel, Charset.defaultCharset(), "\r\n", "\n");
			try {
				StringWriter writer = new StringWriter(1024);
				Assert.assertNotEquals(-1L, drbchannel.read(writer));
				Assert.assertEquals("the first line", writer.toString());
				Assert.assertNotNull(drbchannel.getLastDelimiterRead());

				writer = new StringWriter(1024);
				Assert.assertNotEquals(-1L, drbchannel.read(writer));
				Assert.assertEquals("the last line", writer.toString());
				Assert.assertNull(drbchannel.getLastDelimiterRead());

				writer = new StringWriter(1024);
				Assert.assertEquals(-1L, drbchannel.read(writer));
				Assert.assertNull(drbchannel.getLastDelimiterRead());
			} finally {
				drbchannel.close();
			}
		} finally {
			fchannel.close();
		}
	}
	
	@Test
	public void pomXmlBigChunk() throws IOException, URISyntaxException {
		this.pomXml(1024);
	}
	
	@Test
	public void pomXmlSmallChunk() throws IOException, URISyntaxException {
		this.pomXml(16);
	}
	
	public void pomXml(int chunkSize) throws IOException, URISyntaxException {
		File file = new File("pom.xml");
		Path path = file.toPath();
		FileChannel fchannel = FileChannel.open(path);
		try {
			DelimitedReadableByteChannel drbchannel = new DelimitedReadableByteChannel(chunkSize, fchannel, Charset.defaultCharset(), "\n");
			try {
				StringWriter writer = new StringWriter(1024);
				Assert.assertNotEquals(-1L, drbchannel.read(writer));
				Assert.assertEquals("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", writer.toString());
				Assert.assertEquals("\n", drbchannel.getLastDelimiterRead());

				writer = new StringWriter(1024);
				Assert.assertEquals(1, drbchannel.read(writer));
				Assert.assertEquals("", writer.toString());
				Assert.assertEquals("\n", drbchannel.getLastDelimiterRead());
			} finally {
				drbchannel.close();
			}
		} finally {
			fchannel.close();
		}
	}
	
}
