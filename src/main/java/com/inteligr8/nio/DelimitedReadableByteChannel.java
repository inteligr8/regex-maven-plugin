/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.nio;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.HashSet;
import java.util.Set;

public class DelimitedReadableByteChannel implements ReadableByteChannel {

	private final CharBuffer buffer;
	private final ReadableByteChannel rbchannel;
	private final Charset charset;
	private final Set<String> delimiters;
	private String lastDelimiter;
	
	public DelimitedReadableByteChannel(ReadableByteChannel rbchannel, Charset charset, String... delimiters) {
		this(128, rbchannel, charset, delimiters);
	}
	
	public DelimitedReadableByteChannel(int bufferSize, ReadableByteChannel rbchannel, Charset charset, String... delimiters) {
		this.buffer = CharBuffer.allocate(bufferSize);
		this.buffer.flip();
		
		this.rbchannel = rbchannel;
		this.charset = charset;
		
		this.delimiters = new HashSet<String>(delimiters.length+2);
		for (String delimiter : delimiters)
			this.delimiters.add(delimiter);
	}
	
	@Override
	public void close() throws IOException {
		this.rbchannel.close();
	}
	
	@Override
	public boolean isOpen() {
		return this.rbchannel.isOpen();
	}
	
	@Override
	public int read(ByteBuffer dst) throws IOException {
		return this.rbchannel.read(dst);
	}
	
	public int read(Appendable output, String... additionalDelimiters) throws IOException {
		Set<String> delimiters = this.delimiters;
		if (additionalDelimiters != null && additionalDelimiters.length > 0) {
			delimiters = new HashSet<>(this.delimiters);
			for (String delimiter : additionalDelimiters)
				delimiters.add(delimiter);
		}

		int bytesReadFromBuffer = 0;
		int bytesReadFromChannel = 0;
		this.lastDelimiter = null;
		CharsetDecoder cdecoder = this.charset.newDecoder();
		
		while (bytesReadFromChannel >= 0 && this.lastDelimiter == null) {
			// buffer should always be left in the ready-to-read state
			int bytesToReadFromBuffer = this.buffer.remaining();
			this.readToDelimiter(output);
			bytesReadFromBuffer += bytesToReadFromBuffer - this.buffer.remaining();

			// switch buffer to ready-to-write state
			this.buffer.compact();
			
			ByteBuffer bbuffer = ByteBuffer.allocate(this.buffer.remaining());
			bytesReadFromChannel = this.rbchannel.read(bbuffer);
			
			bbuffer.flip();
			cdecoder.decode(bbuffer, this.buffer, false);
			
			// switch buffer to ready-to-read state
			this.buffer.flip();
		}
		
		if (this.lastDelimiter != null || bytesReadFromBuffer > 0) {
			return bytesReadFromBuffer;
		} else {
			return bytesReadFromChannel;
		}
	}
	
	private void readToDelimiter(Appendable output) throws IOException {
		String sbuffer = this.buffer.toString();
		int pos = -1;
		
		for (String delimiter : this.delimiters) {
			pos = sbuffer.indexOf(delimiter);
			if (pos >= 0) {
				this.lastDelimiter = delimiter;
				this.buffer.position(pos + delimiter.length());
				break;
			}
		}
		
		if (this.lastDelimiter == null) {
			pos = sbuffer.length();
			this.buffer.position(pos);
		}
		output.append(sbuffer, 0, pos);
	}
	
	public String getLastDelimiterRead() {
		return this.lastDelimiter;
	}

}
