/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.model;

public class Regex implements Normalizable {
	
	private String pattern;
	private String replacement;
	private String previousPattern;
	
	public String getPattern() {
		return this.pattern;
	}
	
	public String getReplacement() {
		return this.replacement;
	}
	
	public String getPreviousPattern() {
        return previousPattern;
    }
	
	public Regex setPattern(String pattern) {
		this.pattern = pattern;
		return this;
	}
	
	public Regex setReplacement(String replacement) {
		this.replacement = replacement;
		return this;
	}
	
	public Regex setPreviousPattern(String previousPattern) {
        this.previousPattern = previousPattern;
        return this;
    }
	
	@Override
	public void normalize() {
		if (this.replacement == null)
			this.replacement = "";
	}

}
