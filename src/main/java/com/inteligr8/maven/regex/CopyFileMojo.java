/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

import org.apache.maven.plugins.annotations.Mojo;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "copy-file", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class CopyFileMojo extends AbstractFileMojo {
	
	@Override
	protected void executeOnFile(Path sourcePath, Path targetPath) throws IOException {
		if (CopyFileMojo.this.overwrite) {
			Files.copy(sourcePath, targetPath, StandardCopyOption.REPLACE_EXISTING);
		} else {
			Files.copy(sourcePath, targetPath);
		}
	}
    
}
