/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "match-file", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class MatchFileContentMojo extends AbstractMatchMojo {

	@Parameter( property = "chunkSize", required = true, defaultValue = "1024" )
	protected int chunkSize = 1024;
	
	@Parameter( property = "filesets", required = true )
	protected List<FileSet> filesets;
	
	@Parameter( property = "newProperty", required = true )
	protected String newProperty;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing file regex match");
    	
    	boolean matches = this.matchesContentInFileSet();
    	if (matches)
    		this.getLog().info("Matches!");
    	this.project.getProperties().setProperty(this.newProperty, String.valueOf(matches));
    }
    
    private boolean matchesContentInFileSet() throws MojoExecutionException {
		FileSetManager fsman = new FileSetManager();
		Path basepath = this.project.getBasedir().toPath();
		
		try {
	    	for (FileSet fileSet : this.filesets) {
	    		Path baseInputPath = this.resolveDirectory(basepath, fileSet.getDirectory(), "fileset input");
	    		
	    		String[] filePathsAndNames = fsman.getIncludedFiles(fileSet);
	    		for (String filePathAndName : filePathsAndNames) {
	    			Path file = baseInputPath.resolve(filePathAndName);
	    			if (!Files.isDirectory(file))
	    				if (this.matches(file, this.chunkSize))
	    					return true;
	    		}
	    	}
		} catch (IOException ie) {
			throw new MojoExecutionException("Execution failed due to an I/O related issue", ie);
		}
    	
    	return this.negate;
    }
    
    private Path resolveDirectory(Path basepath, String directory, String errorName) throws IOException, MojoExecutionException {
		Path path = new File(directory).toPath();
		if (!path.isAbsolute())
			path = basepath.resolve(path);
		
		if (!Files.exists(path))
			throw new MojoExecutionException("A " + errorName + " directory does not exist: " + directory);
		
		if (!Files.isDirectory(path))
			throw new MojoExecutionException("A " + errorName + " does reference a directory: " + directory);
		
		return path;
    }
    
    @Override
    protected void validateParamsPreNormalization() throws MojoFailureException {
    	super.validateParamsPreNormalization();
    	
    	if (this.filesets == null || this.filesets.isEmpty())
    		throw new MojoFailureException("At least one 'fileset' is required");
    }
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	for (FileSet fileset : this.filesets) {
    		if (fileset.getDirectory() == null)
    			fileset.setDirectory(this.project.getBasedir().getAbsolutePath());
    	}
    	
    	this.newProperty = StringUtils.trimToNull(this.newProperty);
    }
    
    @Override
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	super.validateParamsPostNormalization();
    	
    	if (this.newProperty == null)
    		throw new MojoFailureException("The 'newProperty' element is required");
    }
    
}
