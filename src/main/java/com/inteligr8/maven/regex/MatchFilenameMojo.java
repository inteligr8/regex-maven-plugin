/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.mutable.MutableBoolean;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "match-filename", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class MatchFilenameMojo extends AbstractMatchMojo {
	
	@Parameter( property = "sourceDirectory", required = false )
	protected File sourceDirectory;
	
	@Parameter( property = "newProperty", required = true )
	protected String newProperty;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing file regex match");
    	
    	final MutableBoolean matches = new MutableBoolean(false);
    	final Path basepath = this.sourceDirectory.toPath();
    	
    	try {
	    	Files.walkFileTree(basepath, new FileVisitor<Path>() {
	    		
	    		@Override
	    		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
	    			return FileVisitResult.CONTINUE;
	    		}

	    		@Override
	    		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
	    			if (exc != null)
	    				throw exc;
	    			return FileVisitResult.CONTINUE;
	    		}
	    		
	    		@Override
	    		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	    			if (!Files.isDirectory(file)) {
	    	        	getLog().debug("Visiting file: " + file);
	    	        	
	    				if (matches(basepath.relativize(file).toString()))
	    					matches.setTrue();
	    			}
	    			return matches.isTrue() ? FileVisitResult.TERMINATE : FileVisitResult.CONTINUE;
	    		}
	    		
	    		@Override
	    		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
	    			throw exc;
	    		}
			});
    	} catch (IOException ie) {
    		throw new MojoExecutionException(ie.getMessage(), ie);
    	}
    	
    	if (matches.booleanValue())
    		this.getLog().info("Matches!");
    	this.project.getProperties().setProperty(this.newProperty, matches.toString());
    }
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	if (this.sourceDirectory == null)
    		this.sourceDirectory = this.project.getBasedir();
    	this.newProperty = StringUtils.trimToNull(this.newProperty);
    }
    
    @Override
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	super.validateParamsPostNormalization();
    	
    	if (this.newProperty == null)
    		throw new MojoFailureException("The 'newProperty' element is required");
    }
    
}
