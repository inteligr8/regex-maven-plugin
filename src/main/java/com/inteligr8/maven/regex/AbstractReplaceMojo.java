/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.File;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import com.inteligr8.maven.model.Regex;
import com.inteligr8.nio.DelimitedReadableByteChannel;

public abstract class AbstractReplaceMojo extends AbstractRegexMojo {
	
	@Parameter( property = "regexes", required = true )
	protected List<Regex> regexes;

	// Pattern does not implement hashCode, so not using a Map
	private List<Triple<Pattern, String, Pattern>> compiledRegexes;
    
	@Override
	protected void executeOnText(Properties props, String text, String newPropertyName) {
		if (text == null)
			text = "";
    	String originalText = text;
    	text = this.replaceAll(text);
    	
		if (this.getLog().isDebugEnabled() && !text.equals(originalText))
        	this.getLog().debug("Manipulated value: " + text);
		if (props == null)
			props = this.project.getProperties();
		props.setProperty(newPropertyName, text);
	}
	
	protected boolean replaceFirst(Path file, Path tofile, int chunkSize) throws IOException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("replace first: " + file + " => " + tofile);
		
		boolean didReplace = false;
		Charset charset = Charset.forName(this.charsetName);
		StringBuilder strbuilder = new StringBuilder();

		FileChannel targetChannel = FileChannel.open(tofile, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		try {
			FileChannel sourceChannel = FileChannel.open(file, StandardOpenOption.READ);
			try {
				DelimitedReadableByteChannel rbchannel = new DelimitedReadableByteChannel(chunkSize, sourceChannel, charset, "\r\n", "\n");
				try {
					while (rbchannel.read(strbuilder) >= 0) {
						if (this.allowMultiLineMatch) {
							// we have to load the whole file into memory
							// this is the problem with the multiline match option
							if (rbchannel.getLastDelimiterRead() != null)
								strbuilder.append(rbchannel.getLastDelimiterRead());
						} else {
							String line = strbuilder.toString();
							if (!didReplace) {
								String processedLine = this.replaceFirst(line);
								if (!line.equals(processedLine))
									didReplace = true;
								line = processedLine;
							}
							
							targetChannel.write(charset.encode(line));
							if (rbchannel.getLastDelimiterRead() != null)
								targetChannel.write(charset.encode(rbchannel.getLastDelimiterRead()));
						}
					}
				} finally {
					rbchannel.close();
				}
			} finally {
				sourceChannel.close();
			}
			
			if (this.allowMultiLineMatch) {
				String processedText = this.replaceFirst(strbuilder.toString());
				targetChannel.write(charset.encode(processedText));
			}
		} finally {
			targetChannel.close();
		}
		
		return didReplace;
	}
	
	protected String replaceFirst(String text) {
		if (text == null)
			text = "";
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("replace first: " + text.length());
		
    	for (Triple<Pattern, String, Pattern> regex : this.compiledRegexes) {
        	this.getLog().debug("Applying regex pattern: " + regex.getLeft());
        	this.getLog().debug("Operating on value: " + text);
        	
    		Matcher matcher = regex.getLeft().matcher(text);
    		if (regex.getRight() == null) {
    		    text = matcher.replaceFirst(regex.getMiddle());
    		} else if (matcher.find()) {
                Integer previousMatchStart = this.findLastMatchIndex(regex.getRight(), text.substring(0, matcher.start()));
    		    text = text.substring(0, previousMatchStart) + text.substring(matcher.start());

                matcher = regex.getLeft().matcher(text);
                text = matcher.replaceFirst(regex.getMiddle());
    		}
    	}
    	
    	return text;
	}
	
	protected boolean replaceAll(Path file, Path tofile, int chunkSize) throws IOException {
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("replace all: " + file + " => " + tofile);
		
		boolean overwrite = file.equals(tofile);
		boolean didReplace = false;
		Charset charset = Charset.forName(this.charsetName);
		StringBuilder strbuilder = new StringBuilder();

		if (overwrite) {
			// if overwriting the existing file, use a temporary file
			tofile = File.createTempFile("regexed-", ".tmp").toPath();
		}
		
		FileChannel targetChannel = FileChannel.open(tofile, StandardOpenOption.WRITE, StandardOpenOption.CREATE, StandardOpenOption.TRUNCATE_EXISTING);
		try {
			FileChannel sourceChannel = FileChannel.open(file, StandardOpenOption.READ);
			try {
				DelimitedReadableByteChannel rbchannel = new DelimitedReadableByteChannel(chunkSize, sourceChannel, charset, "\r\n", "\n");
				try {
					while (rbchannel.read(strbuilder) >= 0) {
						if (this.allowMultiLineMatch) {
							// we have to load the whole file into memory
							// this is the problem with the multiline match option
							if (rbchannel.getLastDelimiterRead() != null)
								strbuilder.append(rbchannel.getLastDelimiterRead());
						} else {
							String line = strbuilder.toString();
							this.getLog().debug("line: " + line);
							String processedLine = this.replaceAll(line);
							if (!didReplace || (this.getLog().isDebugEnabled() && !line.equals(processedLine))) {
								this.getLog().debug("replaced line: " + processedLine);
								didReplace = true;
							}
							
							targetChannel.write(charset.encode(processedLine));
							if (rbchannel.getLastDelimiterRead() != null)
								targetChannel.write(charset.encode(rbchannel.getLastDelimiterRead()));
							
							strbuilder = new StringBuilder(256);
						}
					}
				} finally {
					rbchannel.close();
				}
			} finally {
				sourceChannel.close();
			}
			
			if (this.allowMultiLineMatch) {
				String processedText = this.replaceAll(strbuilder.toString());
				targetChannel.write(charset.encode(processedText));
			}
		} finally {
			targetChannel.close();
		}
		
		if (overwrite) {
			Files.move(tofile, file, StandardCopyOption.REPLACE_EXISTING);
		}
		
		return didReplace;
	}
	
	protected String replaceAll(String text) {
		if (text == null)
			text = "";
		if (this.getLog().isDebugEnabled())
			this.getLog().debug("replace all: " + text.length());
		
    	for (Triple<Pattern, String, Pattern> regex : this.compiledRegexes) {
        	this.getLog().debug("Applying regex pattern: " + regex.getLeft());
        	this.getLog().debug("Operating on value: " + text);
        	
    		Matcher matcher = regex.getLeft().matcher(text);
    		if (regex.getRight() == null) {
    		    text = matcher.replaceAll(regex.getMiddle());
            } else while (matcher.find()) {
                Integer previousMatchStart = this.findLastMatchIndex(regex.getRight(), text.substring(0, matcher.start()));
                text = text.substring(0, previousMatchStart) + text.substring(matcher.start());

                matcher = regex.getLeft().matcher(text);
                text = matcher.replaceFirst(regex.getMiddle());
            }
    	}
    	
    	return text;
	}
    
    private Integer findLastMatchIndex(Pattern pattern, String text) {
        Matcher matcher = pattern.matcher(text);
        
        Integer index = null;
        while (matcher.find())
            index = matcher.start();
        return index;
    }
	
	@Override
	protected void validateParamsPreNormalization() throws MojoFailureException {
		super.validateParamsPreNormalization();
    	
    	if (this.regexes == null)
    		throw new MojoFailureException("A 'regexes' element is required");
    }
    
	@Override
	protected void normalizeParameters() throws MojoFailureException {
		super.normalizeParameters();
    	
		ListIterator<Regex> r = this.regexes.listIterator();
    	while (r.hasNext()) {
    		Regex regex = r.next();
    		if (regex == null) {
    			r.remove();
    		} else {
    			regex.normalize();
    		}
    	}
    }
	
	@Override
	protected void validateParamsPostNormalization() throws MojoFailureException {
		super.validateParamsPostNormalization();

		if (this.regexes.isEmpty())
			throw new MojoFailureException("At least one 'regexes' element is required");
    	
    	this.compiledRegexes = new LinkedList<>();
    	for (Regex regex : this.regexes) {
        	this.getLog().debug("Compiling regex pattern: " + regex.getPattern());
        	try {
	    		Pattern pattern = Pattern.compile(regex.getPattern());
	    		String previousPatternStr = StringUtils.trimToNull(regex.getPreviousPattern());
                Pattern previousPattern = previousPatternStr == null ? null : Pattern.compile(previousPatternStr);
	    		this.compiledRegexes.add(new ImmutableTriple<>(pattern, regex.getReplacement(), previousPattern));
        	} catch (PatternSyntaxException pse) {
        		throw new MojoFailureException("'" + regex.getPattern() + "' is not a valid regular expression: " + pse.getMessage());
        	}
    	}
    }
    
}
