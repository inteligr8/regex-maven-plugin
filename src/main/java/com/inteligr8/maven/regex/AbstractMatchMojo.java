/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

import com.inteligr8.nio.DelimitedReadableByteChannel;

public abstract class AbstractMatchMojo extends AbstractRegexMojo {

	@Parameter( property = "allowPartialMatch", required = true, defaultValue = "true" )
	protected boolean allowPartialMatch = true;
	
	@Parameter( property = "patterns", required = true )
	protected List<String> patterns;
	
	@Parameter( property = "negate", required = true, defaultValue = "false" )
	protected boolean negate = false;

	private List<Pattern> compiledPatterns;
	
	@Override
	protected void executeOnText(Properties props, String text, String newPropertyName) {
		boolean matches = this.matches(text);
		if (matches)
        	this.getLog().debug("Matches!");
		if (props == null)
			props = this.project.getProperties();
		props.setProperty(newPropertyName, String.valueOf(matches));
	}
	
	protected boolean matches(Path file, int chunkSize) throws IOException {
		Charset charset = Charset.forName(this.charsetName);
		StringBuilder strbuilder = new StringBuilder();

		FileChannel sourceChannel = FileChannel.open(file, StandardOpenOption.READ);
		try {
			DelimitedReadableByteChannel rbchannel = new DelimitedReadableByteChannel(chunkSize, sourceChannel, charset, "\r\n", "\n");
			try {
				while (rbchannel.read(strbuilder) >= 0) {
					if (this.allowMultiLineMatch) {
						// we have to load the whole file into memory
						// this is the problem with the multiline match option
						if (rbchannel.getLastDelimiterRead() != null)
							strbuilder.append(rbchannel.getLastDelimiterRead());
					} else {
						String line = strbuilder.toString();
						if (this.matches(line))
							return true;
					}
				}
			} finally {
				rbchannel.close();
			}
		} finally {
			sourceChannel.close();
		}
			
		if (this.allowMultiLineMatch)
			return this.matches(strbuilder.toString());
		return this.negate;
	}
	
	protected boolean matches(String text) {
		if (text == null)
			text = "";
		
    	for (Pattern pattern : this.compiledPatterns) {
        	this.getLog().debug("Applying regex pattern: " + pattern);
        	this.getLog().debug("Operating on value: " + text);
        	
    		Matcher matcher = pattern.matcher(text);
    		if (this.allowPartialMatch) {
    			if (matcher.find())
    				return !this.negate;
    		} else {
    			if (matcher.matches())
    				return !this.negate;
    		}
    	}

    	return this.negate;
    }
	
	@Override
	protected void validateParamsPreNormalization() throws MojoFailureException {
		super.validateParamsPreNormalization();
    	
    	if (this.patterns == null)
    		throw new MojoFailureException("A 'patterns' element is required");
    }
	
	@Override
	protected void normalizeParameters() throws MojoFailureException {
		super.normalizeParameters();
    	
		ListIterator<String> p = this.patterns.listIterator();
    	while (p.hasNext()) {
    		String pattern = p.next();
    		if (pattern == null) {
    			p.remove();
    		}
    	}
    }
	
	@Override
	protected void validateParamsPostNormalization() throws MojoFailureException {
		super.validateParamsPostNormalization();

		if (this.patterns.isEmpty())
			throw new MojoFailureException("At least one 'patterns' element is required");
    	
    	this.compiledPatterns = new LinkedList<>();
    	for (String pattern : this.patterns) {
        	this.getLog().debug("Compiling regex pattern: " + pattern);
        	try {
	    		this.compiledPatterns.add(Pattern.compile(pattern));
        	} catch (PatternSyntaxException pse) {
        		throw new MojoFailureException("'" + pattern + "' is not a valid regular expression: " + pse.getMessage());
        	}
    	}
    }
    
}
