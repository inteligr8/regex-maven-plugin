/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;

public abstract class AbstractFileMojo extends AbstractReplaceMojo {
	
	@Parameter( property = "sourceDirectory", required = false )
	protected File sourceDirectory;

	@Parameter( property = "targetDirectory", required = false )
	protected File targetDirectory;

	@Parameter( property = "overwrite", required = true, defaultValue = "true" )
	protected boolean overwrite = true;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing file regex");
    	
    	final Path sourcePath = this.sourceDirectory.toPath();
    	final Path targetPath = this.targetDirectory.toPath();
    	try {
	    	Files.walkFileTree(sourcePath, new FileVisitor<Path>() {
	    		
	    		@Override
	    		public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
	    			return FileVisitResult.CONTINUE;
	    		}

	    		@Override
	    		public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
	    			if (exc != null)
	    				throw exc;
	    			return FileVisitResult.CONTINUE;
	    		}
	    		
	    		@Override
	    		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
	    			if (!Files.isDirectory(file)) {
	    				String relativePath = sourcePath.relativize(file).toString();
	    				String replacedRelativePath = replaceFirst(relativePath);
	    				if (!relativePath.equals(replacedRelativePath)) {
		    				Path newFile = targetPath.resolve(replacedRelativePath);
		    				if (file.equals(newFile)) {
		    					AbstractFileMojo.this.getLog().error("Relative paths are different, but the resultant paths are the same??");
		    					throw new RuntimeException("This should never happen");
		    				}
		    				
		    				AbstractFileMojo.this.executeOnFile(file, newFile);
	    				}
	    			}
	    			
	    			return FileVisitResult.CONTINUE;
	    		}
	    		
	    		@Override
	    		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {
	    			throw exc;
	    		}
			});
    	} catch (IOException ie) {
    		throw new MojoExecutionException(ie.getMessage(), ie);
    	}
    }
    
    protected abstract void executeOnFile(Path sourcePath, Path targetPath) throws IOException;
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	if (this.sourceDirectory == null)
    		this.sourceDirectory = this.project.getBasedir();
    	if (this.targetDirectory == null)
    		this.targetDirectory = this.project.getBasedir();
    }
    
}
