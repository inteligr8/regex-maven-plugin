/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.util.Properties;

import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.component.annotations.Requirement;

import com.inteligr8.maven.ProjectPropertyResolver;

public abstract class AbstractRegexMojo extends AbstractMojo {
	
	@Parameter( defaultValue = "${project}", readonly = true )
	protected MavenProject project;
	
	@Parameter( defaultValue = "${session}", readonly = true )
	protected MavenSession session;
	
	@Requirement
	private ProjectPropertyResolver propResolver;

	@Parameter( property = "allowMultiLineMatch", required = true, defaultValue = "false" )
	protected boolean allowMultiLineMatch = false;
	
	@Parameter( property = "charset", required = true, defaultValue = "utf-8" )
	protected String charsetName = "utf-8";
	
	@Parameter( property = "skip", required = true, defaultValue = "false" )
	protected boolean skip = false;

    public final void execute() throws MojoExecutionException, MojoFailureException {
    	if (this.skip) {
        	this.getLog().debug("Skipped execution");
    		return;
    	}

    	this.validateParamsPreNormalization();
    	this.normalizeParameters();
    	this.validateParamsPostNormalization();
    	
    	this.go();
    }
    
    protected abstract void go() throws MojoExecutionException, MojoFailureException;

    protected void validateParamsPreNormalization() throws MojoFailureException {
    	this.getLog().debug("Validating parameters before their normalization");
    }

    protected void normalizeParameters() throws MojoFailureException {
    	this.getLog().debug("Normalizing parameters");
    }

    protected void validateParamsPostNormalization() throws MojoFailureException {
    	this.getLog().debug("Validating parameters after their normalization");
    }
    
    protected final void executeOnProperty(String propertyName, String newPropertyName) {
    	this.getLog().debug("Finding property: " + propertyName);
    	
		Properties props = this.propResolver.resolveScope(propertyName);
		if (props == null)
        	this.getLog().debug("Property not found: " + propertyName);

    	String propertyValue = props == null ? null : props.getProperty(propertyName);
    	this.executeOnText(props, propertyValue, newPropertyName);
    }
    
    protected abstract void executeOnText(Properties props, String text, String newPropertyName);
    
}
