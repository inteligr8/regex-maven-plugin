/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "replace-property", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class ReplacePropertyMojo extends AbstractReplaceMojo {
	
	@Parameter( property = "property", required = true )
	protected String property;
	
	@Parameter( property = "newProperty", required = false )
	protected String newProperty;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing property regex replacement");
    	
    	this.executeOnProperty(this.property, this.newProperty);
    }
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	this.property = StringUtils.trimToNull(this.property);
    	this.newProperty = StringUtils.trimToNull(this.newProperty);
    	if (this.newProperty == null)
    		this.newProperty = this.property + "-regex";
    }
    
    @Override
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	super.validateParamsPostNormalization();
    	
    	if (this.property == null)
    		throw new MojoFailureException("The 'property' element is required");
    }
    
}
