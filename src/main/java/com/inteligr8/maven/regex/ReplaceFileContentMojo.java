/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "replace-file", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class ReplaceFileContentMojo extends AbstractReplaceMojo {

	@Parameter( property = "chunkSize", required = true, defaultValue = "1024" )
	protected int chunkSize = 1024;
	
	@Parameter( property = "filesets", required = true )
	protected List<FileSet> filesets;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing file regex replace");
    	
    	this.replaceContentInFileSet();
    }
    
    private boolean replaceContentInFileSet() throws MojoExecutionException {
    	boolean didReplace = false;
		FileSetManager fsman = new FileSetManager();
		Path basepath = this.project.getBasedir().toPath();
		
		try {
	    	for (FileSet fileSet : this.filesets) {
	    		Path baseInputPath = this.resolveDirectory(basepath, fileSet.getDirectory(), false, "fileset input");
	    		Path baseOutputPath = this.resolveDirectory(basepath, fileSet.getOutputDirectory(), true, "fileset output");
	    		
	    		String[] filePathsAndNames = fsman.getIncludedFiles(fileSet);
	    		for (String filePathAndName : filePathsAndNames) {
	    			Path file = baseInputPath.resolve(filePathAndName);
	    			Path tofile = baseOutputPath.resolve(filePathAndName);
	    			if (!Files.exists(tofile.getParent()))
	    				Files.createDirectories(tofile.getParent());
	    			
	    			if (!Files.isDirectory(file))
	    				didReplace = this.replaceAll(file, tofile, this.chunkSize) || didReplace;
	    		}
	    	}
		} catch (IOException ie) {
			throw new MojoExecutionException("Execution failed due to an I/O related issue", ie);
		}
    	
    	return didReplace;
    }
    
    private Path resolveDirectory(Path basepath, String directory, boolean createIfMissing, String errorName) throws IOException, MojoExecutionException {
    	if (directory == null)
    		return this.project.getBasedir().toPath();
    	
		Path path = new File(directory).toPath();
		if (!path.isAbsolute())
			path = basepath.resolve(path);
		
		if (!Files.exists(path)) {
			if (createIfMissing) {
    			Files.createDirectories(path);
			} else {
				throw new MojoExecutionException("A " + errorName + " directory does not exist: " + directory);
			}
		}
		
		if (!Files.isDirectory(path))
			throw new MojoExecutionException("A " + errorName + " does reference a directory: " + directory);
		
		return path;
    }
    
    @Override
    protected void validateParamsPreNormalization() throws MojoFailureException {
    	super.validateParamsPreNormalization();

    	if (this.filesets == null || this.filesets.isEmpty())
    		throw new MojoFailureException("At least one 'fileset' is required");
    }
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	for (FileSet fileset : this.filesets) {
    		if (fileset.getDirectory() == null)
    			fileset.setDirectory(this.project.getBasedir().getAbsolutePath());
    		if (fileset.getOutputDirectory() == null)
    			fileset.setOutputDirectory(fileset.getDirectory());
    	}
    }
    
}
