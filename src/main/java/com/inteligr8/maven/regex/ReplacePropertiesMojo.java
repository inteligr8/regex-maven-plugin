/*
 * This program is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package com.inteligr8.maven.regex;

import java.util.List;
import java.util.ListIterator;

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.codehaus.plexus.component.annotations.Component;

@Mojo( name = "replace-properties", threadSafe = true )
@Component( role = org.apache.maven.plugin.Mojo.class )
public class ReplacePropertiesMojo extends AbstractReplaceMojo {
	
	@Parameter( property = "properties", required = true )
	protected List<String> properties;
	
	@Parameter( property = "newPropertySuffix", required = true, defaultValue = "-regex" )
	protected String propertySuffix;

    @Override
    public void go() throws MojoExecutionException {
    	this.getLog().debug("Executing properties regex replacement");
    	
    	for (String propertyName : this.properties) {
    		this.executeOnProperty(propertyName, propertyName + this.propertySuffix);
    	}
    }
    
    @Override
    protected void validateParamsPreNormalization() throws MojoFailureException {
    	super.validateParamsPreNormalization();
    	
    	if (this.properties == null)
    		throw new MojoFailureException("A 'properties' element is required");
    }
    
    @Override
    protected void normalizeParameters() throws MojoFailureException {
    	super.normalizeParameters();
    	
    	// make sure we have a list and it is nice and neat
		ListIterator<String> p = this.properties.listIterator();
		while (p.hasNext()) {
			String propValue = p.next();
			String trimmedValue = StringUtils.trimToNull(propValue);
			if (propValue == null || trimmedValue == null) {
				p.remove();
			} else if (propValue.length() != trimmedValue.length()) {
				p.remove();
				p.add(trimmedValue);
			} else {
				// nothing to change in the property name
			}
		}
		
		this.propertySuffix = StringUtils.trimToNull(this.propertySuffix);
    }

    @Override
    protected void validateParamsPostNormalization() throws MojoFailureException {
    	super.validateParamsPostNormalization();
    	
    	if (this.propertySuffix == null)
    		throw new MojoFailureException("The 'propertySuffix' element is required");
    	if (this.properties.isEmpty())
    		throw new MojoFailureException("At least one 'property' element is required");
    }
    
}
