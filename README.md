
# Regular Expression Maven Plugin

This is a maven plugin that provides various regular expression based goals to Maven.

## Usage

```xml
<project>
    ...
    <build>
        ...
        <plugins>
            ...
            <plugin>
                <groupId>com.inteligr8</groupId>
                <artifactId>regex-maven-plugin</artifactId>
                <version>...</version>
                <executions>
                    <execution>
                        <id>sample-regex-test</id>
                        <phase>validate</phase>
                        <goals><goal>replace-property</goal></goals>
                        <configuration>
                            <property>a.property.to.eval</property>
                            <regexes>
                                <regex>
                                    <pattern>\.txt<pattern>
                                    <replacement></replacement>
                                </regex>
                            </regexes>
                            <newProperty>a.new.property.to.set</newProperty>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
            ...
        </plugins>
        ...
    </build>
    ...
</project>
```

## Goals

| Goal                 | Description |
| -------------------- | ----------- |
| `replace-file`       | Apply a regular expression pattern against the contents of a file, replacing matches with the regex-compliant replacement text.  The existing file is changed. |
| `replace-properties` | Apply a regular expression pattern against a set of property values, replacing matches with the regex-compliant replacement text, and storing in new properties. |
| `replace-property`   | Apply a regular expression pattern against a property value, replacing matches with the regex-compliant replacement text, and storing in a new property. |
| `replace-text`       | Apply a regular expression pattern against text, replacing matches with the regex-compliant replacement text, and storing in a new property. |
| `match-file`         | Evaluate a regular expression pattern against the contents of a file, storing the result in a boolean property. |
| `match-filename`     | Evaluate a regular expression pattern against the name of a file, storing the result in a boolean property. |
| `match-properties`   | Evaluate a regular expression pattern against a set of property values, storing the result in boolean properties. |
| `match-property`     | Evaluate a regular expression pattern against a property value, storing the result in a boolean property. |
| `match-text`         | Evaluate a regular expression pattern against text, storing the result in a boolean property. |
| `copy-file`          | A convenience goal that copies all files/folders specified in a source path to a target path. |
| `move-file`          | A convenience goal that moves all files/folders specified in a source path to a target path. |

### Goal: `replace-file`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `chunkSize`            | `int`       |          | `1024`  | The streaming buffer/chunk size in bytes, while evaluating the regular expressions.  Adjust for classic speed/memory trade-off.  Be aware that files are processed line-by-line, unless `allowMultiLineMatch` is set to `true`. |
| `filesets`             | `FileSet[]` | Yes      |         | Executes against these files. |
| `regexes`              | `Regex[]`   | Yes      |         | Applies these regular expressions and replacement text against those files. |
| `allowMultiLineMatch`  | `boolean`   |          | `false` | A value of `true` allows for multi-line matching.  This takes substantially more memory/time to process. |
| `charset`              | `string`    |          | `utf-8` | |
| `skip`                 | `boolean`   |          | `false` | |

### Goal: `replace-properties`

| Configuration Property | Data Type  | Required | Default  | Description |
| ---------------------- |:----------:|:--------:| -------- | ----------- |
| `properties`           | `String[]` | Yes      |          | Executes against these properties. |
| `newPropertySuffix`    | `String`   |          | `-regex` | All evaluated properties will be copied to new properties with the same property name plus this suffix. |
| `regexes`              | `Regex[]`  | Yes      |          | Applies these regular expressions and replacement text against those files. |
| `allowMultiLineMatch`  | `boolean`  |          | `false`  | A value of `true` allows for multi-line matching.  This takes substantially more memory/time to process. |
| `charset`              | `string`   |          | `utf-8`  | |
| `skip`                 | `boolean`  |          | `false`  | |

### Goal: `replace-property`

| Configuration Property | Data Type  | Required | Default | Description |
| ---------------------- |:----------:|:--------:| ------- | ----------- |
| `property`             | `String`   | Yes      |         | Executes against this property. |
| `newProperty`          | `String`   |          |         | The evaluated property will be copied to this property name.  If not specified, it will be the same name as the `property` with a `-regex` suffix. |
| `regexes`              | `Regex[]`  | Yes      |         | Applies these regular expressions and replacement text against those files. |
| `allowMultiLineMatch`  | `boolean`  |          | `false` | A value of `true` allows for multi-line matching.  This takes substantially more memory/time to process. |
| `charset`              | `string`   |          | `utf-8` | |
| `skip`                 | `boolean`  |          | `false` | |

### Goal: `replace-text`

| Configuration Property | Data Type  | Required | Default | Description |
| ---------------------- |:----------:|:--------:| ------- | ----------- |
| `text`                 | `String`   | Yes      |         | Execute against this text. |
| `newProperty`          | `String`   | Yes      |         | The evaluated text will be copied to this property name. |
| `regexes`              | `Regex[]`  | Yes      |         | Applies these regular expressions and replacement text against those files. |
| `allowMultiLineMatch`  | `boolean`  |          | `false` | A value of `true` allows for multi-line matching.  This takes substantially more memory/time to process. |
| `charset`              | `string`   |          | `utf-8` | |
| `skip`                 | `boolean`  |          | `false` | |

### Goal: `match-file`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `chunkSize`            | `int`       |          | `1024`  | The streaming buffer/chunk size in bytes, while evaluating the regular expressions.  Adjust for classic speed/memory trade-off.  Be aware that files are processed line-by-line, unless `allowMultiLineMatch` is set to `true`. |
| `filesets`             | `FileSet[]` | Yes      |         | Executes against these files. |
| `newProperty`          | `String`    | Yes      |         | If a single regular expression matches the contents in a single file, then this property will be set to `true`; `false` otherwise. |
| `patterns`             | `String[]`  | Yes      |         | Evaluates these regular expressions against those files. |
| `allowPartialMatch`    | `boolean`   |          | `true`  | A value of `true` allows for partial matching, rather than exact line-by-line matching. |
| `negate`               | `boolean`   |          | `false` | A value of `true` will reverse the `true`/`false` on the resultant property value. |
| `allowMultiLineMatch`  | `boolean`   |          | `false` | A value of `true` allows for multi-line matching.  This takes substantially more memory/time to process. |
| `charset`              | `string`    |          | `utf-8` | |
| `skip`                 | `boolean`   |          | `false` | |

### Goal: `match-filename`

| Configuration Property | Data Type   | Required | Default | Description |
| ---------------------- |:-----------:|:--------:| ------- | ----------- |
| `sourceDirectory`      | `File`      | Yes      |         | Executes against this file. |
| `newProperty`          | `String`    | Yes      |         | If a single regular expression matches the name of the file, then this property will be set to `true`; `false` otherwise. |
| `patterns`             | `String[]`  | Yes      |         | Evaluates these regular expressions the file. |
| `allowPartialMatch`    | `boolean`   |          | `true`  | A value of `true` allows for partial matching, rather than exact filename matching. |
| `negate`               | `boolean`   |          | `false` | A value of `true` will reverse the `true`/`false` on the resultant property value. |
| `charset`              | `string`    |          | `utf-8` | |
| `skip`                 | `boolean`   |          | `false` | |

### Goal: `match-properties`

| Configuration Property | Data Type  | Required | Default  | Description |
| ---------------------- |:----------:|:--------:| -------- | ----------- |
| `properties`           | `String[]` | Yes      |          | Executes against these properties. |
| `newPropertySuffix`    | `String`   |          | `-regex` | All evaluated properties will be copied to new properties with the same property name plus this suffix. |
| `patterns`             | `String[]` | Yes      |          | Evaluates these regular expressions against those properties' values. |
| `allowPartialMatch`    | `boolean`  |          | `true`   | A value of `true` allows for partial matching, rather than exact matching. |
| `negate`               | `boolean`  |          | `false`  | A value of `true` will reverse the `true`/`false` on the resultant property value. |
| `allowMultiLineMatch`  | `boolean`  |          | `false`  | A value of `true` allows for multi-line matching.  Very rare for property evaluation. |
| `charset`              | `string`   |          | `utf-8`  | |
| `skip`                 | `boolean`  |          | `false`  | |

### Goal: `match-property`

| Configuration Property | Data Type  | Required | Default | Description |
| ---------------------- |:----------:|:--------:| ------- | ----------- |
| `property`             | `String`   | Yes      |         | Executes against this property. |
| `newProperty`          | `String`   |          |         | The evaluated property will be copied to this property name.  If not specified, it will be the same name as the `property` with a `-regex` suffix. |
| `patterns`             | `String[]` | Yes      |         | Evaluates these regular expressions against the property value. |
| `allowPartialMatch`    | `boolean`  |          | `true`  | A value of `true` allows for partial matching, rather than exact matching. |
| `negate`               | `boolean`  |          | `false` | A value of `true` will reverse the `true`/`false` on the resultant property value. |
| `allowMultiLineMatch`  | `boolean`  |          | `false` | A value of `true` allows for multi-line matching.  Very rare for property evaluation. |
| `charset`              | `string`   |          | `utf-8` | |
| `skip`                 | `boolean`  |          | `false` | |

### Goal: `match-text`

| Configuration Property | Data Type  | Required | Default | Description |
| ---------------------- |:----------:|:--------:| ------- | ----------- |
| `text`                 | `String`   | Yes      |         | Execute against this text. |
| `newProperty`          | `String`   | Yes      |         | The evaluated text will be copied to this property name. |
| `patterns`             | `String[]` | Yes      |         | Evaluates these regular expressions against the property value. |
| `allowPartialMatch`    | `boolean`  |          | `true`  | A value of `true` allows for partial matching, rather than exact matching. |
| `negate`               | `boolean`  |          | `false` | A value of `true` will reverse the `true`/`false` on the resultant property value. |
| `allowMultiLineMatch`  | `boolean`  |          | `false` | A value of `true` allows for multi-line matching.  Very rare for property evaluation. |
| `charset`              | `string`   |          | `utf-8` | |
| `skip`                 | `boolean`  |          | `false` | |

### Goal: `copy-file`

| Configuration Property | Data Type | Required | Default    | Description |
| ---------------------- |:----------:|:--------:| --------- | ----------- |
| `sourceDirectory`      | `File`    |          | ${basedir} | Copy all files from this directory. |
| `targetDirectory`      | `File`    |          | ${basedir} | Copy all files to this directory. |
| `overwrite`            | `boolean` |          | `true`     | |

### Goal: `move-file`

| Configuration Property | Data Type | Required | Default    | Description |
| ---------------------- |:----------:|:--------:| --------- | ----------- |
| `sourceDirectory`      | `File`    |          | ${basedir} | Move all files from this directory. |
| `targetDirectory`      | `File`    |          | ${basedir} | Move all files to this directory. |
| `overwrite`            | `boolean` |          | `true`     | |

## Model

### Object Type: `Regex`

| Element           | Data Type | Required | Default | Description |
| ----------------- |:---------:|:--------:| ------- | ----------- |
| `pattern`         | `String`  | Yes      |         | A regular expression pattern. |
| `replacement`     | `String`  |          | ``      | A regular expression replacement string. |
| `previousPattern` | `String`  |          |         | A regular expression pattern to look back against, removing everything down to and including the pattern. |

The `previousPattern` allows for you to match some text and remove everything before the match, up to the `prevoiusPattern.  This is useful when matching a method and removing it and all its annotations.
